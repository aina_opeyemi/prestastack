
{if $status == 'ok'}
<p>{l s='Your order on %s is complete.' sprintf=$shop_name mod='prestastack'}
		<br /><br />- {l s='Total amount' mod='prestastack'} <span class="price"><strong>{$total_to_pay}</strong></span>
		{if !isset($reference)}
			<br /><br />- {l s='Your order number is #%d.' sprintf=$id_order mod='prestastack'}
		{else}
			<br /><br />- {l s='Your order reference is %s.' sprintf=$reference mod='prestastack'}
		{/if}		<br /><br />{l s='An email has been sent to you with this information.' mod='prestastack'}
		<br /><br /> <strong>{l s='Your order is currently being processed.' mod='prestastack'}</strong>
		<br /><br />{l s='If you have questions, comments or concerns, please contact our' mod='prestastack'} <a href="{$link->getPageLink('contact', true)|escape:'html'}">{l s='expert customer support team' mod='prestastack'}</a>.
	</p>
{else}
	<p class="warning">
		{l s='We noticed a problem with your order. If you think this is an error, feel free to contact our' mod='prestastack'} 
		<a href="{$link->getPageLink('contact', true)|escape:'html'}">{l s='expert customer support team' mod='prestastack'}</a>.
	</p>
{/if}
