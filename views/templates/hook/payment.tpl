<div class="row">
    <div class="col-xs-12">
        <p class="payment_module">
            <a href="{$prestastack_url|escape:'html'}" title="{l s='Pay by credit/debit card' mod='prestastack'}" class="card" style="padding-left:10px;">
                <img src="{$prestastack_path|escape:'htmlall':'UTF-8'}img/payment-logo.png" alt="{l s='Master and visacard accepted here.' mod='prestastack'}" />
                {l s='Pay with credit/debit card' mod='prestastack'}&nbsp;<span>{l s='(securely processed by paystack)' mod='prestastack'}</span><img src="{$prestastack_path}/logo.png" alt="{l s='Pay by credit/debit card' mod='prestastack'}" width="30"/>
            </a>

        </p>
    </div>
</div>
