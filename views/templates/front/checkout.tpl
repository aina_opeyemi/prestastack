{capture name=path}{l s='Place Order' mod='prestastack'}{/capture}

<h1 class="page-heading">{l s='Place Order' mod='prestastack'}</h1>

{if isset($error)}
<div class="alert alert-warning">
    <p>{$error}</p>
    {l s='Please, contact the store owner. ' mod='prestastack'} {*<a href="{$link->getPageLink('contact')}">{l s='Contact us' mod='bluesnap'}</a>*}
</div>

{/if}
{assign var='current_step' value='payment'}
{include file="./shopping-cart.tpl"}

<form action="{$link->getModuleLink('prestastack', 'checkout', [], true)|escape:'htmlall':'UTF-8'}" method="post" class="prestastack_purchase_form">
    <div class="box cheque-box">
        <h3 class="page-subheading"><img src="{$this_path|escape:'htmlall':'UTF-8'}/logo.png" alt="{l s='PayStack payment' mod='prestastack'}" /> {l s='PayStack payment' mod='prestastack'}</h3>
        <input type="hidden" name="confirm" value="1" />
        <div class="prestastack_container">
            <div>
                <p>{l s='You have chosen to pay with Paystack.' mod='prestastack'}</p>
                <p>{l s='The total amount of your order is' mod='prestastack'}
                    <span id="amount_{$currencies.0.id_currency|intval}" class="price prestastack_price">{convertPrice price=$total}</span>.
                    {if $use_taxes == 1}
                        {l s='(tax incl.)' mod='prestastack'}
                    {/if}
                </p>
            </div>
            <div class="clear"></div>
        </div>
        <p>
            <b>{l s='To complete your purchase, you’ll be directed to a secure order page with local payment options displayed based on your country' mod='prestastack'}.</b>
        </p>
    </div>

    <p class="cart_navigation clearfix" id="cart_navigation">
        <a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" class="button-exclusive btn btn-default">
            <i class="icon-chevron-left"></i>{l s='Other payment methods' mod='prestastack'}
        </a>
        <button type="submit" class="button btn btn-default button-medium">
            <span>{l s='Continue to secure order form' mod='prestastack'}<i class="icon-chevron-right right"></i></span>
        </button>
    </p>
    {*<p class="cart_navigation">
        <a href="{$link->getPageLink('order', true)|escape:'htmlall':'UTF-8'}?step=3" class="button_large">{l s='Other payment methods' mod='prestastack'}</a>
        <input type="submit" name="submit" value="{l s='Continue to secure order form' mod='prestastack'}" class="exclusive_large" />
    </p>*}
</form>