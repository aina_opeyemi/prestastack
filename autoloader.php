<?php

if (!defined('_PS_VERSION_'))
    exit;

require _PS_MODULE_DIR_.'prestastack/lib/paystack/src/Paystack.php';
\Yabacon\Paystack::registerAutoloader();