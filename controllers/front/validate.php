<?php


require_once _PS_MODULE_DIR_.'prestastack/autoloader.php';

/**
 * Class PrestastackValidateModuleFrontController
 * Handles callback from paystack
 */
class PrestastackValidateModuleFrontController extends ModuleFrontController {


    /**
     *
     */
    public function initContent()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::initContent();

        if (Tools::getIsset('trxref')){

            $reference= Tools::getValue('trxref');

            //verify the transaction
            $paystack = new \Yabacon\Paystack(Configuration::get('PAYSTACK_MODE') ? Configuration::get('PAYSTACK_PRIVATE_KEY_LIVE') : Configuration::get('PAYSTACK_PRIVATE_KEY_TEST'));
            $response = $paystack->transaction->verify([
                'reference' => $reference,
            ]);

            if(! (stristr($response->data->status, 'success') && stristr($response->message, 'success')))
                Tools::redirect('index.php?controller=order&step=1');

            $auth_code = $response->data->authorization->authorization_code;
            $last_four = $response->data->authorization->last4;
            $exp_month = $response->data->authorization->exp_month;
            $exp_year = $response->data->authorization->exp_year;
            $cc_type = $response->data->authorization->card_type;

            $transaction = Db::getInstance()->getRow(
                'SELECT * FROM '._DB_PREFIX_.'paystack_transactions WHERE reference = \''.pSQL($reference).'\''
            );

            if(! $transaction['reference'])
                Tools::redirect('index.php?controller=order&step=1');

            $customer = new Customer($transaction['id_customer']);
            $cart = $this->context->cart;
            $currency = $this->context->currency;
            $total = (float)$transaction['amount'];

            if (!Validate::isLoadedObject($customer))
                Tools::redirect('index.php?controller=order&step=1');

            Db::getInstance()->update("paystack_transactions", [
                'status' => 'paid',
                'cc_exp' => pSQL($exp_month.'/'.$exp_year),
                'cc_last_four' => pSQL($last_four),
                'cc_type' => pSQL($cc_type)
            ], 'reference = \''.pSQL($reference).'\'');

            Db::getInstance()->insert("paystack_customers", [
                'id_customer' => (int)$transaction['id_customer'],
                'auth_code' => pSQL($auth_code),
                'cc_last_four' => pSQL($last_four),
                'date_add' => pSQL('NOW()')
            ]);

            $message = $this->module->l('Paystack Transaction Details:')."\n\n".
                $this->module->l('Paystack Transaction reference:').' '.$reference."\n".
                $this->module->l('Amount:').' '.$total ."\n".
                $this->module->l('Processed on:').' '.strftime('%Y-%m-%d %H:%M:%S', time())."\n".
                $this->module->l('Currency:').' '. $currency->iso_code."\n".
                $this->module->l('Credit card:').' '.$cc_type.' ('.$this->module->l('Exp.:').' '.$exp_month.'/'.$exp_year.')'."\n".
                $this->module->l('Last 4 digits:').' '.sprintf('%04d', $last_four)."\n".
                $this->module->l('Mode:').' '.(Configuration::get('PAYSTACK_MODE') ? $this->module->l('Live') : $this->module->l('Test'))."\n";

            $this->module->validateOrder($cart->id, Configuration::get('PAYSTACK_PAYMENT_ORDER_STATUS'), $total, $this->module->displayName, $message, [], (int)$currency->id, false, $customer->secure_key);
            if (version_compare(_PS_VERSION_, '1.5', '>='))
            {
                $new_order = new Order((int)$this->module->currentOrder);
                if (Validate::isLoadedObject($new_order))
                {
                    $payment = $new_order->getOrderPaymentCollection();
                    if (isset($payment[0]))
                    {
                        $payment[0]->transaction_id = pSQL($reference);
                        $payment[0]->save();
                    }
                }
            }
            Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);

        } else {
            Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');
        }
    }

    
}