# Paystack
![Paystack](/logo.png)

Basic payment module that allows you to accept payments with [Paystack][1] in PrestaShop with

### Current features
- Process credit card payments with Stripe

### Todo
- Optionally allow paystack inline payments
- Save

## Installation
### Module installation
- Upload the module through FTP or your Back Office
- Install the module
- Configure the module and enter your paystack secret key and public keys
- Remember to set the mode to live or test on the configuration page
- Goodluck!

## Compatibility
This module is compatible these versions:
- `1.5.1.*`, `1.6.1.*`

The module is **NOT** compatible with Cloud

## Minimum requirements
- PrestaShop `1.5.1.0`
- PHP `5.4`
- enabled cURL extension for PHP.

## License
Academic Free License 3.0

[1]: https://www.paystack.com