<?php

/**
 * Created by PhpStorm.
 * User: barmmie
 * Date: 22/08/2016
 * Time: 1:18 PM
 */

if (!defined('_PS_VERSION_'))
    exit;



class PrestaStack extends PaymentModule
{

    protected $hooks = array(
        'displayHeader',
        'payment',
        'adminOrder',
        'BackOfficeHeader',
        'displayOrderConfirmation',
        'actionObjectCurrencyUpdateBefore',
    );

    public function __construct()
    {

        $this->name = 'prestastack';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Dev Cyber';
        $this->need_instance = 1;
        $this->is_configurable = 1;
        $this->bootstrap = true;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        parent::__construct();

        $this->displayName = $this->l('Prestashop for paystack');
        $this->description = $this->l('Paystack payment module for prestashop. Allows payment via mastercard, visa et al)');
        $this->confirmUninstall = $this->l('IRREVERSIBLE! Are you sure you want to uninstall? This would delete all paystack payment/card data from tour database');
    }

    public function install()
    {

        $installed = parent::install() &&
            $this->registerHook('payment') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('paymentReturn') &&
            $this->setDefaultConfigValues() &&
            $this->migrateDb();


        return $installed;
    }

    public function uninstall()
    {
        $uninstalled = parent::uninstall() &&
                $this->unregisterHook('payment') &&
                $this->unregisterHook('backOfficeHeader') &&
                $this->unregisterHook('paymentReturn') &&
                $this->removeDefaultConfigValues() &&
                $this->rollbackDb();

        return $uninstalled;
    }

    public function migrateDb()
    {

        $db = Db::getInstance();
        return $db->Execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'paystack_customers` (
                `id_paystack_customers` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `paystack_id` varchar(32) NOT NULL, 
                `auth_code` varchar(32) NOT NULL, 
                `id_customer` int(10) unsigned NOT NULL,
                `cc_last_four` int(11) NOT NULL, 
                `date_add` datetime NOT NULL, 
                PRIMARY KEY (`id_paystack_customers`), 
                KEY `id_customer` (`id_customer`),
                KEY `auth_code` (`auth_code`)) 
                ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1') &&
        $db->Execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'paystack_transactions` (
        `id_paystack_transactions` int(11) NOT NULL AUTO_INCREMENT,
		`id_customer` int(10) unsigned NOT NULL, 
		`id_cart` int(10) unsigned NOT NULL,
		`id_order` int(10) unsigned DEFAULT NULL, 
		`reference` varchar(32) NOT NULL,
		`status` enum(\'paid\',\'unpaid\') NOT NULL,
		`currency` varchar(3) DEFAULT NULL, 
		`cc_type` varchar(16) DEFAULT NULL, 
		`cc_exp` varchar(8) DEFAULT NULL, 
		`cc_last_four` int(11) DEFAULT NULL,
		`cvc_check` tinyint(1) NULL DEFAULT \'0\', 
		`amount` decimal(10,2) NOT NULL, 
		`mode` enum(\'live\',\'test\') NOT NULL,
		`date_add` datetime NOT NULL, 
		PRIMARY KEY (`id_paystack_transactions`), 
		KEY `idx_transaction` (`id_cart`,`status`))
		ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
    }

    public function rollbackDb()
    {
        $db =  Db::getInstance();

       return $db->Execute('DROP TABLE `'._DB_PREFIX_.'paystack_customers`')
       && Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'paystack_transactions`');

    }

    public function setDefaultConfigValues()
    {
        return Configuration::updateValue('PAYSTACK_MODE', 0) &&
        Configuration::updateValue('PAYSTACK_PENDING_ORDER_STATUS', (int)Configuration::get('PS_OS_PAYMENT')) &&
        Configuration::updateValue('PAYSTACK_PAYMENT_ORDER_STATUS', (int)Configuration::get('PS_OS_PAYMENT'));
    }

    public function removeDefaultConfigValues()
    {
        return Configuration::deleteByName('PAYSTACK_PUBLIC_KEY_TEST') &&
        Configuration::deleteByName('PAYSTACK_PUBLIC_KEY_LIVE')&&
        Configuration::deleteByName('PAYSTACK_MODE') &&
        Configuration::deleteByName('PAYSTACK_PRIVATE_KEY_TEST') &&
        Configuration::deleteByName('PAYSTACK_PRIVATE_KEY_LIVE') &&
        Configuration::deleteByName('PAYSTACK_PAYMENT_ORDER_STATUS');
    }

    public function hookPayment($params)
    {

        if (!$this->active)
            return;

        $prestastack_url = $this->context->link->getModuleLink('prestastack', 'checkout', array(), true);
        $this->smarty->assign(array(
            'prestastack_url' => $prestastack_url,
            'prestastack_path' => $this->_path,
        ));

        return $this->display(__FILE__, 'payment.tpl');
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active)
            return;

        $state = $params['objOrder']->getCurrentState();
        if (in_array($state, array(Configuration::get('PAYSTACK_PAYMENT_ORDER_STATUS'))))
        {
            $this->smarty->assign(array(
                'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                'status' => 'ok',
                'id_order' => $params['objOrder']->id
            ));
            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
                $this->smarty->assign('reference', $params['objOrder']->reference);
        }
        else
            $this->smarty->assign('status', 'failed');
        return $this->display(__FILE__, 'payment_return.tpl');
    }

    public function getTemplate($area, $file)
    {
        return 'views/templates/'.$area.'/'.$file;
    }


    public function hasValidPaystackSettings()
    {
        if (Configuration::get('PAYSTACK_MODE'))
            return Configuration::get('PAYSTACK_PUBLIC_KEY_LIVE') != '' && Configuration::get('PAYSTACK_PRIVATE_KEY_LIVE') != '';
        else
            return Configuration::get('PAYSTACK_PUBLIC_KEY_TEST') != '' && Configuration::get('PAYSTACK_PRIVATE_KEY_TEST') != '';
    }

    
    public function displayForm() {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $statuses = OrderState::getOrderStates((int)$this->context->cookie->id_lang);

        $fields_form[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Paystack Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'radio',

                    'values' => array(
                        array(
                            'id' => 'Live',
                            'value' => 1,
                            'label' => $this->l('Live')
                        ),
                        array(
                            'id' => 'Test',
                            'value' => 0,
                            'label' => $this->l('Test')
                        )
                    ),
                    'label' => $this->l('Paystack Mode'),
                    'name' => 'paystack_mode',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Test Secret Key'),
                    'name' => 'paystack_private_key_test',
                    'required' => true
                ),

                array(
                    'type' => 'text',
                    'label' => $this->l('Test Public Key'),
                    'name' => 'paystack_public_key_test',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Live Secret Key'),
                    'name' => 'paystack_private_key_live',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Live Public Key'),
                    'name' => 'paystack_public_key_live',
                    'required' => true
                ),

                array(
                    'type' => 'select',
                    'options' => array(
                        'query' => $statuses,
                        'id' => 'id_order_state',
                        'name' => 'name'
                    ),
                    'label' => $this->l('Order status in case of successful payment'),
                    'name' => 'paystack_payment_status',
                    'required' => true
                ),


            ),

            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                        '&token='.Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        $helper->fields_value['paystack_mode'] = Configuration::get('PAYSTACK_MODE');
        $helper->fields_value['paystack_public_key_test'] = Configuration::get('PAYSTACK_PUBLIC_KEY_TEST');
        $helper->fields_value['paystack_public_key_live'] = Configuration::get('PAYSTACK_PUBLIC_KEY_LIVE');
        $helper->fields_value['paystack_private_key_test'] = Configuration::get('PAYSTACK_PRIVATE_KEY_TEST');
        $helper->fields_value['paystack_private_key_live'] = Configuration::get('PAYSTACK_PRIVATE_KEY_LIVE');
        $helper->fields_value['paystack_payment_status'] = Configuration::get('PAYSTACK_PAYMENT_ORDER_STATUS');
        return $helper->generateForm($fields_form);

    }

    public function displayErrors($errors) {
        $output = '';

        foreach ($errors as $error) {
            $output .= $this->displayError($error);
        }

        return $output;
    }

    public function postProcessedForm()
    {
        if (Tools::isSubmit('submit'.$this->name))
        {
                $configuration_values = array(
                    'PAYSTACK_MODE' => Tools::getValue('paystack_mode'),
                    'PAYSTACK_PUBLIC_KEY_TEST' => trim(Tools::getValue('paystack_public_key_test')),
                    'PAYSTACK_PUBLIC_KEY_LIVE' => trim(Tools::getValue('paystack_public_key_live')),
                    'PAYSTACK_PRIVATE_KEY_TEST' => trim(Tools::getValue('paystack_private_key_test')),
                    'PAYSTACK_PRIVATE_KEY_LIVE' => trim(Tools::getValue('paystack_private_key_live')),
                    'PAYSTACK_PAYMENT_ORDER_STATUS' => (int)Tools::getValue('paystack_payment_status')
                );

                foreach ($configuration_values as $configuration_key => $configuration_value) {
                    Configuration::updateValue($configuration_key, $configuration_value);

                }

                Tools::redirectAdmin('index.php?tab=AdminModules&conf=4&configure='.$this->name.
                    '&token='.Tools::getAdminToken('AdminModules'.
                        (int)Tab::getIdFromClassName('AdminModules').(int)$this->context->employee->id));

        }

    }


    /**
     * Admin backend content
     * @return string
     */
    public function getContent()
    {
        $this->postProcessedForm();

        return $this->displayForm();

    }
}